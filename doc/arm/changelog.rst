.. Copyright (C) Internet Systems Consortium, Inc. ("ISC")
..
.. SPDX-License-Identifier: MPL-2.0
..
.. This Source Code Form is subject to the terms of the Mozilla Public
.. License, v. 2.0.  If a copy of the MPL was not distributed with this
.. file, you can obtain one at https://mozilla.org/MPL/2.0/.
..
.. See the COPYRIGHT file distributed with this work for additional
.. information regarding copyright ownership.

.. _changelog:

Changelog
=========

.. note:: The following list contains detailed information about BIND 9
   development. Regular users should refer to :ref:`Release Notes <relnotes>`
   for changes relevant to them.

.. include:: ../changelog/changelog-9.18.34-S1.rst
.. include:: ../changelog/changelog-9.18.34.rst
.. include:: ../changelog/changelog-9.18.33-S1.rst
.. include:: ../changelog/changelog-9.18.33.rst
.. include:: ../changelog/changelog-9.18.32-S1.rst
.. include:: ../changelog/changelog-9.18.32.rst
.. include:: ../changelog/changelog-9.18.31-S1.rst
.. include:: ../changelog/changelog-9.18.31.rst
.. include:: ../changelog/changelog-9.18.30-S1.rst
.. include:: ../changelog/changelog-9.18.30.rst
.. include:: ../changelog/changelog-9.18.29-S1.rst
.. include:: ../changelog/changelog-9.18.29.rst
.. include:: ../changelog/changelog-history.rst
